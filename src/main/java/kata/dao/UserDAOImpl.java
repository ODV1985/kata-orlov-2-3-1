package kata.dao;

import kata.entity.MyUser;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.List;

@Repository
public class UserDAOImpl implements UserDao {


    //PersistenceContext
    //1. Гарантия того, что сущности будут сохранены в БД.
    //2. Отслеживает изменения и отправляет их в БД.
    //3. Работает как кеш
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<MyUser> getAllEmployee() {
        return entityManager.createQuery("from MyUser", MyUser.class).getResultList();
    }

    @Override
    public MyUser findUser(Integer id) {
        return entityManager.find(MyUser.class, id);
    }

    @Override
    public void add(MyUser myUser) {
        entityManager.persist(myUser);
    }

    @Override
    public void deleteUser(Integer id) {
        MyUser myUser = findUser(id);
        entityManager.remove(myUser);
    }

    @Override
    public void updateUser(MyUser myUser) {
        entityManager.merge(myUser);
    }

}
